import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SomeTestComponent } from './shared/components/some-test/some-test.component';
import { BuildingBlockComponent } from './shared/components/building-block/building-block.component';

@NgModule({
  declarations: [
    AppComponent,
    SomeTestComponent,
    BuildingBlockComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
