import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { BuildingBlockComponent } from './shared/components/building-block/building-block.component';


const routes: Routes = [
  {path: 'bb', component: BuildingBlockComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
