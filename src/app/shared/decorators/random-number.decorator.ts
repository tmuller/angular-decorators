
export interface RandomNumberInterface {
  rndInt: number;
}

export function RandomNumber(max: number): ClassDecorator {
  // tslint:disable-next-line:only-arrow-functions
  return function( constructor: any ) {
    //
    // const component = constructor.name;
    // const original = constructor.prototype;

    // tslint:disable-next-line:only-arrow-functions
    const randomNumber = Math.round(Math.random() * max);
    console.log('randomNumber', randomNumber);
    constructor.prototype.rndInt = randomNumber;
  };
}
