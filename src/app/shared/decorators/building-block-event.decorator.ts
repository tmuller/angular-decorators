import { Observable, Subject } from 'rxjs';
import { BuildingBlockEvent } from '../interfaces/building-block.interface';


export interface BuildingBlockInterface {
  blockEvent: Observable<any>;
  bbOnEvent: any;
  emitEvent: any;
}

export const BuildingBlock: () => ClassDecorator = (function() {

  const eventSubject = new Subject<BuildingBlockEvent<any>>();

  return function buildingBlockFunctionality(): ClassDecorator {
    return function(constructor: any) {
      const component = constructor.name;
      let eventSubscription;


      constructor.prototype.blockEvent = eventSubject.asObservable();
      constructor.prototype.bbOnEvent = function(rxOnNext: (x: BuildingBlockEvent<any>) => {}) {
        eventSubscription = constructor.prototype.blockEvent.subscribe(x => {
          rxOnNext(x);
        });
      };
      const sourceOnDestroy = constructor.prototype.ngOnDestroy;
      constructor.prototype.ngOnDestroy = function( ...args ) {
        eventSubscription.unsubscribe();
        // tslint:disable-next-line:no-unused-expression
        !!sourceOnDestroy && sourceOnDestroy.apply(this, args);
      };

      constructor.prototype.emitEvent = function(evt) {
        eventSubject.next(evt);
      };
    };
  };
}());


// export function BuildingBlock(): ClassDecorator {
//   // tslint:disable-next-line:only-arrow-functions
//   return function( constructor: any ) {
//     // if( !environment.production ) {
//       // You can add/remove events for your needs
//       const component = constructor.name;
//
//       constructor.prototype._eventSubject = new Subject<BuildingBlockEvent<any>>();
//       constructor.prototype.blockEvent = constructor.prototype._eventSubject.toObservable();
//
//       constructor.prototype.blockEvent.subscribe( x => {console.log(x)});
//
//
//
//       // LIFECYCLE_HOOKS.forEach(hook => {
//       //   const original = constructor.prototype[hook];
//       //
//       //   constructor.prototype[hook] = function ( ...args ) {
//       //     console.log(`%c ${component} - ${hook}`, `color: #4CAF50; font-weight: bold`, ...args);
//       //     original && original.apply(this, args);
//       //   }
//       // });
//     // }
//
//   };
// }
