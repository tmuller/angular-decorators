import { Component, OnInit } from '@angular/core';
import { BuildingBlock, BuildingBlockInterface } from '../../decorators/building-block-event.decorator';
import { RandomNumber, RandomNumberInterface } from '../../decorators/random-number.decorator';

// tslint:disable:no-empty-interface
export interface SomeTestComponent extends BuildingBlockInterface {}
export interface SomeTestComponent extends RandomNumberInterface {}

@Component({
  selector: 'app-some-test',
  templateUrl: './some-test.component.html',
  styleUrls: ['./some-test.component.sass']
})
@RandomNumber(400)
@BuildingBlock()
export class SomeTestComponent implements OnInit {

  constructor() {}

  ngOnInit() {
    this.bbOnEvent(x => {
      console.log('in SomeTestComponent', x);
    });
  }

}
