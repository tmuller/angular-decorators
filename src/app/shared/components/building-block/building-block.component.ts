import { Component, OnInit } from '@angular/core';
import { BuildingBlock, BuildingBlockInterface } from '../../decorators/building-block-event.decorator';
import { BuildingBlockEvent } from '../../interfaces/building-block.interface';

// tslint:disable-next-line:no-empty-interface
export interface BuildingBlockComponent extends BuildingBlockInterface {}

@Component({
  selector: 'app-building-block',
  templateUrl: './building-block.component.html',
  styleUrls: ['./building-block.component.sass']
})
@BuildingBlock()
export class BuildingBlockComponent implements OnInit {

  constructor() { }

  public ngOnInit(): void {
    this.bbOnEvent(this.onBuildingBlockEvent);
  };

  public onBuildingBlockEvent(evt: BuildingBlockEvent<any>): void {
    console.log('evt', evt);
  }

  public actionClicked($event) {
    const event: BuildingBlockEvent<any> = {
      completed: true,
      connected: false,
    };

    this.emitEvent(event);
  }

}
