export interface BuildingBlockEvent<StateType> {
  completed?: boolean;
  connected?: boolean;
  state?: StateType
}
